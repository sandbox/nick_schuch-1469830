<?php
/**
 * @file
 * dhc_videos.features.inc
 */

/**
 * Implements hook_views_api().
 */
function dhc_videos_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function dhc_videos_node_info() {
  $items = array(
    'dhc_video' => array(
      'name' => t('Drupal Helpful Content: Video'),
      'base' => 'node_content',
      'description' => t('Drupal Help Content video content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
